
import React, { useState, useEffect } from 'react'
import { View, StyleSheet, Text, Button, SafeAreaView } from 'react-native'
import EmailField from '../Components/EmailField'
import PasswordField from '../Components/PasswordField'

import RoundedButton from '../Components/RoundedButton'




const UserSignInScreen = ({ navigation }) => {
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')

    return (
        <SafeAreaView>
            <View>
                <Text style={styles.titleText}>Sign In</Text>
                <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                    <EmailField
                        term={email}
                        placeHolder="Email Address"
                        onTermChange={newEmail => setEmail(newEmail)} />

                    <PasswordField
                        term={password}
                        placeHolder="Password"
                        onTermChange={newPassword => setPassword(newPassword)} />

                    <RoundedButton buttonStyle={styles.LoginButtonStyle} title="Log in" textStyle={styles.signinTextColorStyle} />
                </View>
            </View>
        </SafeAreaView>
    )
}

UserSignInScreen.navigationOptions = () => {
    return {
        headerShown: false
    }
}
const styles = StyleSheet.create({
    titleText: {
        fontSize: 30,
        fontWeight: 'bold',
        marginHorizontal: 30,
        marginBottom: 30,
        marginTop: 40,
        // fontFamily: 'Thonburi-Light'

    },
    LoginButtonStyle: {
        backgroundColor: 'black',
    },
    signinTextColorStyle: {
        color: 'white',
    },

})
export default UserSignInScreen