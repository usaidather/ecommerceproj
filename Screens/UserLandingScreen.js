
import React, { useState, useEffect } from 'react'
import { View, StyleSheet, Text, Button, SafeAreaView, Image } from 'react-native'
import RoundedButton from '../Components/RoundedButton'


const UserLandingScreen = ({ navigation }) => {

    return (
        <SafeAreaView>
            <View style={styles.ViewStyle}>
                <Image source={require('../assets/UserModule/amazon_PNG5.png')} />
                <Text style={styles.WelcomeStyle}>Welcome to Amazon Store</Text>
                <Text style={styles.WelcomeDetailStyle}>Shop and get updates on new products and sales with our mobile app.</Text>
                <RoundedButton buttonStyle={styles.LoginButtonStyle} title="Login" textStyle={styles.signinTextColorStyle} />
                <RoundedButton buttonStyle={styles.SignupButtonStyle} title="Sign Up" textStyle={styles.signupTextColorStyle} />

            </View>
        </SafeAreaView>
    )
}

UserLandingScreen.navigationOptions = () => {
    return {
        headerShown: false
    }
}

const styles = StyleSheet.create({
    ViewStyle: {
        marginTop: 100 * 0.5,
        justifyContent: 'center',
        alignItems: 'center',
    },

    WelcomeStyle: {
        marginHorizontal: 20,
        marginTop: 20,
        fontSize: 35,
        fontWeight: 'bold',
        textAlign: 'center'
    },

    WelcomeDetailStyle: {
        marginHorizontal: 40,
        marginVertical: 20,
        fontSize: 14,
        fontWeight: '100',
        textAlign: 'center'


    },

    LoginButtonStyle: {
        backgroundColor: 'black',
    },
    SignupButtonStyle: {
        backgroundColor: 'white'
    },
    signinTextColorStyle: {
        color: 'white',
    },
    signupTextColorStyle: {
        color: 'black'
    }

})

export default UserLandingScreen