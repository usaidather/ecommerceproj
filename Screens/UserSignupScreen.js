
import React, { useState, useEffect } from 'react'
import { View, StyleSheet, Text, Button, SafeAreaView, Dimensions } from 'react-native'
import EmailField from '../Components/EmailField'
import PasswordField from '../Components/PasswordField'
import RoundedTextField from '../Components/RoundedTextField'

import RoundedButton from '../Components/RoundedButton'
const { width, height } = Dimensions.get('window');

const UserSignupScreen = ({ navigation }) => {
    return (
        <SafeAreaView>
            <View>
                <Text style = {styles.titleText}>Sign up</Text>
                <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                    <RoundedTextField placeHolder = "Full Name"/>
                    <EmailField placeHolder="Email Address" />
                    <RoundedTextField placeHolder = "Phone Number"/>
                    <PasswordField placeHolder="Password" />
                    <RoundedButton buttonStyle={styles.LoginButtonStyle} title="Sign up" textStyle={styles.signinTextColorStyle} />
                </View>
            </View>
        </SafeAreaView>
    )
}

UserSignupScreen.navigationOptions = () => {
    return {
        headerShown: false
    }
}
const styles = StyleSheet.create({
    titleText: {
        fontSize: 30,
        fontWeight: 'bold',
        marginHorizontal: 30,
        marginBottom: 30,
        marginTop: height *0.2,
        // fontFamily: 'Thonburi-Light'

    },
    LoginButtonStyle: {
        backgroundColor: 'black',
    },
    signinTextColorStyle: {
        color: 'white',
    },

})
export default UserSignupScreen