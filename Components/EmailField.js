import React , { useState } from 'react'
import { TextInput, Text, StyleSheet, Dimensions, View } from 'react-native'
import PasswordField from './PasswordField';

const { width, height } = Dimensions.get('window');
const EmailField = ({ term, placeHolder, onTermChange, onTermSubmit }) => {

const [error, setError] = useState('')
    isEmailValid = () => {
        let email = term
        let pattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
        pattern.test(String(email).toLowerCase()) ? setError('') : setError('Invalid Email Address')
    }

    return (
        <View>
            <Text style={styles.ErrorText}> {error}</Text>
            <View style={styles.TextFieldView}>
                <TextInput
                    autoCorrect={false}
                    style={styles.TextField}
                    placeholder={placeHolder}
                    value={term}
                    onChangeText={onTermChange}
                    onEndEditing={isEmailValid } />
            </View>
        </View>
    )
}


const styles = StyleSheet.create({
    TextField: {
        fontSize: 14,
        flex: 1,
        marginHorizontal: 20
    },
    TextFieldView: {
        height: height * 0.07,
        width: width * 0.8,
        borderRadius: (height * 0.075) / 2,
        marginTop: 5,
        marginBottom: 10,
        borderColor: 'black',
        borderWidth: 1,
        justifyContent: 'center',
    },
    ErrorText: {
        fontSize: 12,
        color: 'red',
        marginHorizontal: 20,

    }

})
export default EmailField