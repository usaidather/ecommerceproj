import { createAppContainer, createSwitchNavigator } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';

import UserLandingScreen from './Screens/UserLandingScreen'
import UserSignInScreen from './Screens/UserSignInScreen'
import UserSignUpScreen from './Screens/UserSignupScreen'


const switchNavigator = createSwitchNavigator({
  loginFlow: createStackNavigator({
    Signin: UserSignInScreen,
    Signup: UserSignUpScreen,
    LandingScreen: UserLandingScreen,

  }),
  // mainFlow: createBottomTabNavigator({
  //   TrackListFlow: createStackNavigator({
  //     TrackList: TrackListScreen,
  //     TrackDetail: TrackDetailScreen,
  //   }),
  //   TrackCreate: TrackCreateScreen,
  //   Account: AccountScreen
  // })
})

const App = createAppContainer(switchNavigator)
export default App